Welcome to pep20_analyzer! If you are looking for info how to use my program, please open INSTALL.md file.
Remember, that my program works ONLY FOR PYTHON 3 CODES!

ORIGINAL ZEN

Beautiful is better than ugly.
    Explicit is better than implicit.
    Simple is better than complex.
    Complex is better than complicated.
    Flat is better than nested.
    Sparse is better than dense.
    Readability counts.
    Special cases aren't special enough to break the rules.
    Although practicality beats purity.
    Errors should never pass silently.
    Unless explicitly silenced.
    In the face of ambiguity, refuse the temptation to guess.
    There should be one-- and preferably only one --obvious way to do it.
    Although that way may not be obvious at first unless you're Dutch.
    Now is better than never.
    Although never is often better than *right* now.
    If the implementation is hard to explain, it's a bad idea.
    If the implementation is easy to explain, it may be a good idea.
    Namespaces are one honking great idea -- let's do more of those!

WHAT DOES MY PROGRAM COMPUTE:

My program calculates the following for given .py modules.
            1) name_convention_rate:
                "There should be one-- and preferably only one --obvious way to do it."
                "Readability counts. Although practicality beats purity." ~ YES!! I STILL FIND JAVA CONVENTIONS BETTER!

                How do I do it: I check the name of every function and method in the desired .py module. I'm checking, if the coder used python name convention
                and how many names are not wirtten in this convention. The coefficient is used to make a rate.

            2) code_complexity_rate:
                "Explicit is better than implicit.
                Simple is better than complex.
                Complex is better than complicated.
                Flat is better than nested."

                How do I do it: Cyclomatic Complexity corresponds to the number of decisions a block of code contains plus 1.
                This number (also called McCabe number) is equal to the number of linearly independent paths through the code. T
                his number can be used as a guide when testing conditional logic in blocks.

            3) code_spread_rate:
                "Sparse is better than dense."

                I calculate the ration between lines of code (instructions) and the amount of functions and methods.

            4) code_description_rate:
                "If the implementation is hard to explain, it's a bad idea.
                If the implementation is easy to explain, it may be a good idea."

                How do I do it: Using radon I simply check the percentage ratio of comments in a .py file. The more, the better (but without exaggeration).

            5) maintainability_rate:
                "Now is better than never.
                Although never is often better than *right* now."
                "In the face of ambiguity, refuse the temptation to guess."

                How do I do it: Radon allowes us to calculate so called maintainability. Maintainability Index is a software metric which measures how maintainable
                (easy to support and change) the source code is. The maintainability index is calculated as a factored formula consisting of SLOC (Source Lines Of Code),
                Cyclomatic Complexity and Halstead volume. All those things may be acquired by radon API.
