import radon.complexity, radon.metrics, radon.visitors
from pep20_analyzer.repo_manager import RepoManager
import os
import logging


def name_convention_rate(name_convention):
    if name_convention < 0.05:
        return 5
    elif name_convention < 0.1:
        return 4
    elif name_convention < 0.15:
        return 3
    elif name_convention < 0.2:
        return 2
    else:
        return 1

def code_complexity_rate(code_complexity):
    # Values chosen after analysing the documentation of radon
    if code_complexity < 5:
        return 5
    elif code_complexity < 10:
        return 4
    elif code_complexity < 25:
        return 3
    elif code_complexity < 35:
        return 2
    else:
        return 1

def code_spread_rate(lines_per_function):
    # I assume, that every 4 lines per function means one rank less
    if lines_per_function == 0:
        return 0
    N = 4
    res = int(6 - (lines_per_function//N))
    return res if res > 0 else 0

def code_description_rate(comments_percentage):
            # Code must be commented well, but too many comments decresaes readability
    if comments_percentage < 5 or comments_percentage > 75:
        return 1
    elif comments_percentage < 10 or comments_percentage > 65:
        return 2
    elif comments_percentage < 20 or comments_percentage > 55:
        return 3
    elif comments_percentage < 30 or comments_percentage > 50:
        return 4
    else:
        return 5

def maintainability_rate(maintainability):
    # I use the radon documentation for every rank
    if maintainability > 25:
        return 5
    elif maintainability > 20:
        return 4
    elif maintainability > 15:
        return 3
    elif maintainability > 10:
        return 2
    else:
        return 1

class PepAnalyser():
    repo_manager = RepoManager()
    logger = logging.getLogger('pep_analizer_logger')

    def analyse_file(self, filename):
        """
        This method takes a filename of the .py python module and analyses it in terms of PEP20

        :param filename(str) - the name of the file you would like to analyse:
        :return (lines_per_function(float), (cyclomatic_complexity, comments_percentage, maintainability), name_convention(float)):
        """

        strCode = PepAnalyser.repo_manager.code_to_str(filename)
        # get data from radon,cyclomatic
        try:
            cyclomatic_data = self._calculate_cyclomatic(strCode)
            metrics_data = self._calculate_metrics(strCode)
            name_convention = self._check_name_convenctions(strCode)
        except SyntaxError:
            PepAnalyser.logger.error('The file ' + filename + ' is not a valid python 3 code.')
            print( 'The file ' + filename + ' is not a valid python 3 code.')
            exit()

        return self.get_ratings(cyclomatic_data, metrics_data, name_convention)

    def get_all_python_files(self, dirname, directories):
        dir_content = os.listdir(dirname)
        for file in dir_content:
            # look for python files or directories
            if(file.endswith('.py')):
                directories.append(dirname + '/' + file)
            elif os.path.isdir(dirname + '/' + file):
                self.get_all_python_files(dirname + '/' + file, directories)

    def analyse_dir(self, dirname):
        list_of_files = []
        self.get_all_python_files(dirname, list_of_files)
        analyse_result = []
        for file in list_of_files:
            analyse_result.append(self.analyse_file(file))

        avg_convention = 0
        avg_complexity = 0
        avg_spread = 0
        avg_description = 0
        avg_maintain = 0
        avg_rate = 0
        for res in analyse_result:
            avg_convention += res[0]
            avg_complexity += res[1]
            avg_spread += res[2]
            avg_description += res[3]
            avg_maintain += res[4]
            avg_rate += res[5]

        # I use lambda to make math like rounding
        round_res = lambda x: int(x) if x - int(x) < 0.5 else int(x) + 1
        return (round_res(avg_convention/len(analyse_result)),
                round_res(avg_complexity/len(analyse_result)),
                round_res(avg_spread/len(analyse_result)),
                round_res(avg_description/len(analyse_result)),
                round_res(avg_maintain/len(analyse_result)),
                round(avg_rate/len(analyse_result), 2))






    def _calculate_cyclomatic(self, strCode):
        PepAnalyser.logger.info(" calculating cyclomatic data")
        # We are interested only in functions and methods itself, I must use list in order to use len()
        cyclomatic_data = [x for x in radon.complexity.cc_visit(strCode) if type(x) == radon.visitors.Function]
        total_function_lines = sum(function[3] - function[1] + 1 for function in cyclomatic_data)
        total_function_number = len(cyclomatic_data)
        if total_function_number != 0:
            lines_per_function = round(total_function_lines/total_function_number,2)
        else:
            lines_per_function = 0
        return lines_per_function


    def _calculate_metrics(self, strCode):
        """
        This method uses radon to calculate data connected with code metrics

        :param strCode: the code in string format
        :return (halstead_volume, cyclomatic_complexity, logical_lines_of_code, comments_percentage, maintainability):
        """
        PepAnalyser.logger.info(" calculating metrics data")
        halstead_volume, cyclomatic_complexity, logical_lines_of_code, comments_percentage = radon.metrics.mi_parameters(strCode)
        # The coefficient, which says how easy it is going to maintain the code in the future. Based on the average depth, number of operands, operations etc.
        maintainability = radon.metrics.mi_compute(halstead_volume, cyclomatic_complexity, logical_lines_of_code, comments_percentage)
        return (cyclomatic_complexity, comments_percentage, maintainability)

    def _check_name_convenctions(self, strCode):
        PepAnalyser.logger.info(" checking name conventions")
        """
        Method uses radon.cyclomatic to check wether the coder uses pure and beatiful python convenction, or ugly java one
        :return float
        """
        # I take into account only functions and methods
        cyclomatic_data = [x[0] for x in radon.complexity.cc_visit(strCode) if type(x) == radon.visitors.Function]
        java_style_sum = 0
        for x in cyclomatic_data:
            if x.islower():
                java_style_sum += 1
        if len(cyclomatic_data) != 0:
            return round(java_style_sum / len(cyclomatic_data), 2)
        else:
            return 0

    def get_ratings(self, cyclomatic_data, metrics_data, name_convention):
        """
        This method calculates ratings for 5 ZEN aspects:
            1) name_convention_rate:
                "There should be one-- and preferably only one --obvious way to do it."
                "Readability counts. Although practicality beats purity." ~ YES!! I STILL FIND JAVA CONVENTIONS BETTER!
            2) code_complexity_rate:
                Explicit is better than implicit.
                Simple is better than complex.
                Complex is better than complicated.
                Flat is better than nested."
            3) code_spread_rate:
                "Sparse is better than dense."
            4) code_description_rate:
                "If the implementation is hard to explain, it's a bad idea.
                If the implementation is easy to explain, it may be a good idea."
            5) maintainability_rate:
                "Now is better than never.
                Although never is often better than *right* now."
                "In the face of ambiguity, refuse the temptation to guess."

        :return: a tuple of all 5 rates + overall code rating (1 to 5, an avarage of all the ratings)
        """

        # order like in the method docstring
        ranks = [name_convention_rate(name_convention),
                 code_complexity_rate(metrics_data[0]),
                 code_spread_rate(cyclomatic_data),
                 code_description_rate(metrics_data[1]),
                 maintainability_rate(metrics_data[2])]

        PepAnalyser.logger.info(' calculated all the ratings')
        overall_rank = sum(ranks)/(len(ranks) - ranks.count(0))
        return ranks + [overall_rank]







