import os
from git import Repo
from git import GitCommandError
import requests
import radon.complexity
import radon.metrics
import logging

class RepoManager():
    """
    This class alowes you to easily manage the code (download it from the url raw file, clone repo to desired directory and
    convert modules into string and ast formats. You can use it as standalone module in your own programs.
    """
    logger = logging.getLogger("repo_manager_logger")
    file_not_found_error = 'There is no such file! Please check the file directory (remember, it must be relative to the main.py)'

    def cloneRepo(self, url, relative_dir):
        # use os.path.realpath to get the absolute path of the given relative one
        savepath = os.path.realpath(relative_dir)
        try:
            repo = Repo.clone_from(url, savepath)
        except GitCommandError:
            RepoManager.logger.error('The directory /repos exists. Please move it or delete it from the program directory or given url is invalid')
            print('The directory /repos exists. Please move it or delete it from the program directory or given url is invalid')
            exit()
        RepoManager.logger.info(' repo ' + url + ' successfully cloned to ' + relative_dir)

    def downloadCodeFromURL(self, url):
        if not os.path.exists('downloaded_code'):
            os.makedirs('downloaded_code')
        r = requests.get(url)
        try:
            with open('downloaded_code/code_tmp.py', 'w', encoding='utf-8') as file:
                file.write(r.text)
        except FileNotFoundError:
            RepoManager.logger.error(RepoManager.file_not_found_error)
            print(RepoManager.file_not_found_error)
            exit()

    def code_to_ast(self, filename):
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                ast = radon.complexity.code2ast(file.read())
                return ast
        except (FileNotFoundError, IsADirectoryError):
            RepoManager.logger.error(RepoManager.file_not_found_error)
            print(RepoManager.file_not_found_erro)
            exit()
        RepoManager.logger.info('Code from ' + filename + ' has been successfully converted to ast object')

    def code_to_str(self, filename):
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                return file.read()
        except (FileNotFoundError, IsADirectoryError):
            RepoManager.logger.error(RepoManager.file_not_found_error)
            print(RepoManager.file_not_found_error)
            exit()
        RepoManager.logger.info('Code from ' + filename + ' has been successfully converted to ast object')

