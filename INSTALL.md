REQUIREMENTS:

- radon (pip3 install radon)
- GitPython (pip3 install gitpython)
- requests (pip3 install requests)

HOW TO RUN:
Please type 'python3 main.py -h' in your console to run the help of the program. You can also use the pep20_analyzer package as
a standalone part (for your own applications). Please note you are able to analyse only python 3 files.

