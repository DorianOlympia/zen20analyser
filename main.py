from pep20_analyzer import repo_manager, pep_analyser
from math import ceil
import argparse
import logging

RATINGS = ('Well, it doesnt have any functions!', 'hell no', 'nope', 'kinda', 'yup', 'hell yeah')
LOG_FILENAME = 'logfile'
logger = logging.getLogger('main_logger')


def exec2():
    # instantiate two basic managers
    manager = repo_manager.RepoManager()
    analyser = pep_analyser.PepAnalyser()
    # set the logger
    logging.basicConfig(filename=LOG_FILENAME,level=logging.INFO)
    logger.setLevel(0)
    logger.info("\n\n---------Program started-----------")

    # the setup of my parser
    parser = argparse.ArgumentParser(description='Checks the desired python 3 modules in terms of ZEN20.')
    parser.add_argument('rel_dir', metavar='DIR', nargs='?',
                   help='a relative directory to the file you want to check OR url to the raw text file (for instance from github) if you use it with the --url argument'
                        'or a clone url to the repo you would like to test')

    parser.add_argument('--url', dest='run_desired', action='store_const',
                   const=anal_git, default=anal_file,
                   help='Downloads desired .py module and analyses it (please provide a valid url to the raw text file).'
                        'Currently this program can download code !only from github repositories!. If you want to download code'
                        'from other repos (like stash), please do it manualy.')

    parser.add_argument('--repo', dest='run_desired', action='store_const',
                   const=anal_rep, default=anal_file,
                   help='Downloads desired github repository and analyses it (please provide a valid clone url).'
                        'Please check if the directory of this program contains repos/ directory'
                        'If so, please delete it or move to another directory')

    parser.add_argument('--dir', dest='run_desired', action='store_const',
                   const=anal_dir, default=anal_file,
                   help='Analyses all the .py files in desired directory')

    args = parser.parse_args()
    logger.info(" arguments successfully parsed")
    # run desired contains a function, which represents a selected option
    args.run_desired(analyser, manager, args.rel_dir)
    logger.info(" desired mode: " + args.run_desired.__name__ +  " selected")
    logger.info(" program successfully executed!")

# Avaliable options

def anal_file(analyser, manager, path):
    ratings = analyser.analyse_file(path)
    logger.info(" local + " + path + " file successfully analised")
    show_ratings(ratings)

def anal_git(analyser, manager, url):
    manager.downloadCodeFromURL(url)
    ratings = analyser.analyse_file('downloaded_code/code_tmp.py')
    logger.info(" remote file " + url + " successfully analised")
    show_ratings(ratings)

def anal_rep(analyser, manager, url):
    manager.cloneRepo(url, 'downloaded_repo/')
    ratings = analyser.analyse_dir('downloaded_repo/')
    show_ratings(ratings)

def anal_dir(analyser, manager, path):
    ratings = analyser.analyse_dir(path)
    show_ratings(ratings)

def show_ratings(ratings):
    print('***** ***** ***** ***** *****')
    print('Are the functions well named, so that it doesn''t \nirritate hardcore python developers?: ', RATINGS[ratings[0]], '|rate: ', ratings[0])
    print('\nIs the code easy to read? Is it not unduly deep and complex?: ', RATINGS[ratings[1]], '|rate: ', ratings[1])
    print('\nDid the programmer divide code between functions? \nAre those functions small and easy to read?: ', RATINGS[ratings[2]], '|rate: ', ratings[2])
    print('\nIs the code well commented? Is it also not overcommented?: ', RATINGS[ratings[3]], '|rate: ', ratings[3])
    print('\nEventually, does it have any perspectives? \nWill it be possible to handle it in the future: ', RATINGS[ratings[4]], '|rate: ', ratings[4])
    print('\n\nWell... Is it a good example of ZEN code then? \nDid the creator write it according to PEP20 rules: ',
          RATINGS[int(ceil(ratings[5]))], '|rate: ', ratings[5])



if __name__ == '__main__':
    exec2()


